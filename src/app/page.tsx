export default function Home() {
  // const background = 'info'
  // ${
  //           background === 'info'
  //             ? 'bg-info'
  //             : background === 'success'
  //             ? 'bg-success'
  //             : background === 'warning'
  //             ? 'bg-warning'
  //             : background === 'error'
  //             ? 'bg-error'
  //             : ''
  //         }
  return (
    <>
      <main className={`w-full h-full `}>
        <div className="w-full h-full flex items-center justify-center">
          <h1 className="-translate-y-3/4">Code Base</h1>
        </div>
      </main>
    </>
  )
}
