import './globals.sass'

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html>
      <body className="w-full h-full">{children}</body>
    </html>
  )
}
