/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  mode: 'jit',
  content: ['./src/**/*.{js,ts,jsx,tsx}', './src/app/**/*.{js,ts,jsx,tsx}', './src/stories/**/*.{ts,tsx}'],
  darkMode: 'class',
  theme: {
    extend: {
      container: {},
      screens: {
        ...defaultTheme.screens,
        'sm': { 'min': '320px', 'max': '480px' },
        'md': { 'min': '480px', 'max': '768px' },
        'lg': { 'min': '769px', 'max': '1024px' },
        'xl': { 'min': '1025px', 'max': '1200px' },
      },
      fontFamily: {
        ...defaultTheme.fontFamily,
        sans: ['Graphik', 'sans-serif'],
      },
      fontSize: {
        ...defaultTheme.fontSize,
      },
      colors: {
        ...defaultTheme.colors,
        'transparent': 'transparent',
        'info': 'rgb(var(--color-info) / <alpha-value>)',
        'success': 'rgb(var(--color-success) / <alpha-value>)',
        'warning': 'rgb(var(--color-warning) / <alpha-value>)',
        'error': 'rgb(var(--color-error) / <alpha-value>)',
      },
      opacity: {
        ...defaultTheme.opacity,
      },
    },
  },
}
